﻿using Rh.Senai.RH.Dao;
using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cpf :"+ txtCpfFuncionario.Text);
        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            // validacao
            // verifica se os campos obrigatorios foram preenchidos
            if (string.IsNullOrEmpty(txtNomeFuncionario.Text)
                || string.IsNullOrEmpty(txtCpfFuncionario.Text)
                || string.IsNullOrEmpty(txtRgFuncionario.Text)
                || string.IsNullOrEmpty(txtEmailFuncionario.Text)
                || string.IsNullOrEmpty(txtTelefoneFuncionario.Text))

            {
                MessageBox.Show("preencha os campos !");
            }
            else
            {


                // instancia um funcionário
                Funcionario funcionario = new Funcionario();

                // obtem o id do funcionario
                // se o textbox de id de funcionario não estiver vazio
                // se foi preenchiado o textbox de IDFuncionario
                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    //cria um id
                    long id = 0;
                    // converte o texto do textbox para long
                    // armazenamento na variavel id
                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        // atribui o id ao objeto funcionario
                        funcionario.ID = id;
                    }
                }

                // atribui dados ao funcionario
                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.Cpf = txtCpfFuncionario.Text;
                funcionario.Rg = txtRgFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelefoneFuncionario.Text;

                // instancia o dao funcionarios
                FuncionarioDao dao = new FuncionarioDao();

                // salva o funcionario no banco de dados
                dao.Salvar(funcionario);

                PreencheDados();

                LimparFormulario();

            }// fim do evento de click
        }

        //método do programador
        private void LimparFormulario()
        {
            txtNomeFuncionario.Clear();
            txtCpfFuncionario.Clear();
            txtRgFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtTelefoneFuncionario.Clear();
            txtIDFuncionario.Clear();
            txtCpfFuncionario.Focus();



        }
        

        private void txtTelefoneFuncionario_TextChanged(object sender, EventArgs e)
        {

        }

        private void FuncionarioForm_Load(object sender, EventArgs e)
        {
            PreencheDados();
        }
        private void PreencheDados()
        {
            // instancia um dao
            FuncionarioDao dao = new FuncionarioDao ();

            // preenche o data grid view
            dgvFuncionarios.DataSource = dao.Consultar();

            // oculta algumas colunas
            dgvFuncionarios.Columns["ID"].Visible = false;
            dgvFuncionarios.Columns["RG"].Visible = false;

            // limpa a seleção do data grid view
            dgvFuncionarios.ClearSelection();

            // limpa os campos formularios
            LimparFormulario();

        }// fim do evento de click


        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if(dgvFuncionarios.CurrentRow != null)
            {
                // pega p id e coloca no texbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3
                    ].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();
            }
        }

        private void btnexcluirFuncionario_Click(object sender, EventArgs e)
        {
            // verifica se o textox de id de funcionario é ou vazio
            //se sim isso significa que nenhum funcionario foi selecionado na lista
            if (String.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = " selecione um funcionario na lista abaixo !";
                string titulo = "operacao nao realizada...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // instancia um funcionario
                Funcionario funcionario = new Funcionario();

                // cria um id funcionario
                long id = 0;

                // converte a tsrong do text box da id para long
                if (long.TryParse(txtIDFuncionario.Text, out id ))
                {
                   // atribui o id ao id do funcionario 
                    funcionario.ID = id;

                }
                //instancia uma dao
                FuncionarioDao dao = new FuncionarioDao();

                // resposta do usuario
                DialogResult resposta = MessageBox.Show("Deseja realmente exluir esse funcionario?","Mensagem...",MessageBoxButtons.YesNo,MessageBoxIcon.Question );

                if (resposta.Equals(DialogResult.Yes))
                {
                    // eclui o funcionario
                    dao.Excluir(funcionario);
                    
                    // atualiza o data grid view
                    PreencheDados();
                }                                           
            }
        }
    }// fim da classe
}
