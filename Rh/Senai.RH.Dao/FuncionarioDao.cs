﻿using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {
        //atributos
        // conexao com o banco de dados
        private SqlConnection connection;
        // instrução sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // instrução messagebox 
        private string titulo = null;

        // contsutor
        public FuncionarioDao()
        {
            // cria um conexao com o banco de dados
            connection  = new ConnectionFactory().GetConnection();
        }
        // metodos
        public List<Funcionario> Consultar()
        {
            // comando sql de consulta
            sql = "Select * FROM Funcionario";

            // lista  de funcionarios cadastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                // abre a conexao com o banco de dados
                connection.Open();
                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                // cria um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                //enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.Cpf = leitor["Cpf"].ToString();
                    funcionario.Rg = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();
                    // adiciona o funcionário na lista de funcionarios
                    funcionarios.Add(funcionario);
                }// fim do while
            }
            catch (SqlException ex)
            {
                msg = " erro ao consultar os funcionarios cadastrados: :" + ex.Message;
                titulo = "erro";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

              }
            finally
            {
                connection.Close();
            }
            return funcionarios;
        }

        public void Excluir(Funcionario funcionario)
        {
            // instrução sql
            sql = "Delete from Funcionario Where IDFuncionario = @IDFuncionario";
            try
            {
                //abre a conexao com o banco de dados 
                connection.Open();
                // cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                // adiciona valor ao parametro @IDFuncionario
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);
                // executa o comando sql do banco de dados
                cmd.ExecuteNonQuery();

                // mendagem de feedback
                msg = "funcionario" + funcionario.Nome + " Excluido com sucesso";
                // titulo da mensagem de feedback
                titulo = "sucesso...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch(SqlException ex)
            {
                // mensagem de fodback
                msg = " erro ao excluir o funcionario !"+ ex.Message;
                // titulo da mensagem de erro
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // fecha a conexao com o baco de dados
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {
            // verifica se o id do funcionario é diferente de 0
            if (funcionario.ID!=0)
            {
                //update 
                sql = "UPDATE Funcionario SET Nome=@Nome,Cpf=@Cpf,Rg=@Rg, Email=@Email, Telefone=@Telefone Where IDFuncionario = @IDFuncionario";
            }
            else
            {
                // insert
                sql = "INSERT INTO Funcionario(Nome, Cpf, Rg, Email, Telefone) VALUES(@Nome, @Cpf, @Rg, @Email, @Telefone )";
            }
            try
            {
              

                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.Cpf);
                cmd.Parameters.AddWithValue("@Rg", funcionario.Rg);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);

                // executa o insert
                cmd.ExecuteNonQuery();
                msg = "Funcionario"+ funcionario.Nome + "salvo com sucesso!";
                titulo = "Sucesso..";

                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
            catch(SqlException ex)
            {
                msg = " Erro o salvar o funcionario!" + ex;
                    titulo = " erro";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }

        }
    }
}
